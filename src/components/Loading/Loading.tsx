import { Box, Spinner } from "@chakra-ui/react";
import React from "react";

export const Loading = () => (
  <Box>
    <Spinner color="yellow.500" size="xl" speed="0.65s" thickness="4px" />
  </Box>
);
