import { Avatar, Box, HStack, Text } from "@chakra-ui/react";
import React from "react";

interface UserProfileProps {
  name?: string;
  image?: string;
  email?: string;
}

export const UserProfile = ({ name, image, email }: UserProfileProps) => {
  return (
    <HStack spacing="3" ps="2">
      <Avatar name={name} src={image} boxSize="10" />
      <Box>
        <Text fontWeight="medium" fontSize="sm">
          {name}
        </Text>
        <Text fontSize="sm">{email}</Text>
      </Box>
    </HStack>
  );
};
