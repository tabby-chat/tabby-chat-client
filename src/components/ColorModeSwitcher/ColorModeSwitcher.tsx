import React from "react";
import {
  Button,
  useColorMode,
  useColorModeValue,
  ButtonProps,
} from "@chakra-ui/react";
import { faMoon, faSun } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

type ColorModeSwitcherProps = Omit<ButtonProps, "aria-label">;

export const ColorModeSwitcher: React.FC<ColorModeSwitcherProps> = (props) => {
  const { toggleColorMode } = useColorMode();
  const text = useColorModeValue("dark", "light");

  const SwitchIcon = useColorModeValue(faMoon, faSun);

  return (
    <Button
      aria-label={`Switch to ${text} mode`}
      fontSize="lg"
      leftIcon={<FontAwesomeIcon icon={SwitchIcon} />}
      marginLeft="2"
      onClick={toggleColorMode}
      size="md"
      {...props}
    >
      Switch to {text} mode
    </Button>
  );
};
