import {
  Input,
  InputGroup,
  InputLeftElement,
  useColorModeValue,
} from "@chakra-ui/react";
import { IconProp } from "@fortawesome/fontawesome-svg-core";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React from "react";

type FilterProps = {
  icon?: IconProp;
  label: string;
};

export const Filter = ({ icon, label }: FilterProps) => (
  <>
    <InputGroup>
      {icon && (
        <InputLeftElement pointerEvents="none">
          <FontAwesomeIcon icon={icon} />
        </InputLeftElement>
      )}
      <Input
        bg={useColorModeValue("gray.200", "gray.900")}
        borderColor="transparent"
        fontSize="sm"
        placeholder={label ? label : "Filter"}
      />
    </InputGroup>
  </>
);
