import { Box, BoxProps, useColorModeValue } from "@chakra-ui/react";
import React from "react";

export const Card = (props: BoxProps) => (
  <Box
    bg={useColorModeValue("gray.300", "gray.700")}
    boxShadow={useColorModeValue("sm", "sm-dark")}
    borderRadius="lg"
    minH="3xs"
    {...props}
  />
);
