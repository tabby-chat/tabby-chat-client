import {
  Box,
  Flex,
  useBreakpointValue,
  useColorModeValue,
} from "@chakra-ui/react";
import React from "react";
import { Outlet } from "react-router-dom";

import { Navbar } from "./Navbar/Navbar";
import { Sidebar } from "./Sidebar/Sidebar";
import Socket from "../features/Socket/Socket";

function Layout() {
  const isDesktop = useBreakpointValue({ base: false, lg: true });

  return (
    <Flex
      as="section"
      bg="white"
      direction={{ base: "column", lg: "row" }}
      height="100vh"
      overflowX="hidden"
      overflowY="auto"
    >
      {isDesktop ? <Sidebar /> : <Navbar />}
      <Socket />
      <Box
        bg={useColorModeValue("gray.300", "gray.800")}
        pt={{ base: "0", lg: "3" }}
        flex="1"
      >
        <Box
          bg={useColorModeValue("gray.50", "gray.700")}
          borderTopLeftRadius={{ base: "none", lg: "2rem" }}
          height="full"
        >
          <Outlet />
        </Box>
      </Box>
    </Flex>
  );
}

export default Layout;
