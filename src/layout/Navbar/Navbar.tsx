import {
  Box,
  Drawer,
  DrawerContent,
  DrawerOverlay,
  Flex,
  Spacer,
  useDisclosure,
  useColorModeValue,
} from "@chakra-ui/react";
import React from "react";

import { Sidebar } from "../Sidebar/Sidebar";
import { ToggleButton } from "./ToggleButton";

export const Navbar = () => {
  const { isOpen, onToggle, onClose } = useDisclosure();

  return (
    <Box
      bg={useColorModeValue("gray.300", "gray.800")}
      width="full"
      py="4"
      px={{ base: "4", md: "8" }}
    >
      <Flex justify="space-between">
        <Spacer />
        <ToggleButton
          isOpen={isOpen}
          aria-label="Open Menu"
          onClick={onToggle}
        />
        <Drawer
          isFullHeight
          isOpen={isOpen}
          onClose={onClose}
          placement="left"
          preserveScrollBarGap
          trapFocus={false}
        >
          <DrawerOverlay />
          <DrawerContent>
            <Sidebar />
          </DrawerContent>
        </Drawer>
      </Flex>
    </Box>
  );
};
