import {
  Box,
  Button,
  ButtonProps,
  Flex,
  IconButton,
  Spacer,
  Text,
} from "@chakra-ui/react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { IconProp } from "@fortawesome/fontawesome-svg-core";
import { faXmark } from "@fortawesome/free-solid-svg-icons";
import React from "react";
import { Link as RouterLink } from "react-router-dom";

interface NavButtonProps extends ButtonProps {
  closeable?: boolean;
  icon: IconProp;
  label: string;
  path: string;
}

export const NavButton = ({ closeable, icon, label, path }: NavButtonProps) => {
  return (
    <Button
      as={RouterLink}
      justifyContent="flex-start"
      leftIcon={<FontAwesomeIcon icon={icon} />}
      py={4}
      role="group"
      to={path}
      w="full"
      variant="ghost"
    >
      <Flex w="full">
        <Box>
          <Text py={4}>{label}</Text>
        </Box>
        <Spacer />
        {closeable && (
          <Box>
            <IconButton
              aria-label="remove"
              display="none"
              opacity="50%"
              isRound
              size="sm"
              variant="ghost"
              _groupHover={{
                display: "block",
              }}
            >
              <FontAwesomeIcon icon={faXmark} />
            </IconButton>
          </Box>
        )}
      </Flex>
    </Button>
  );
};
