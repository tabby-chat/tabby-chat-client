import {
  Divider,
  Flex,
  Stack,
  Text,
  useColorModeValue,
} from "@chakra-ui/react";
import {
  faArrowRightFromBracket,
  faCog,
  faQuestionCircle,
  faUser,
  faUsers,
} from "@fortawesome/free-solid-svg-icons";
import React, { useEffect } from "react";

import { Filter, UserProfile } from "../../components";
import { NavButton } from "./NavButton";

import { useAppDispatch, useAppSelector } from "../../app/hooks";
import {
  selectChatList,
  getChatList,
} from "../../features/ChatList/chatListSlice";
import { selectUserDetails } from "../../features/User/userSlice";

export const Sidebar = () => {
  const dispatch = useAppDispatch();
  const user = useAppSelector(selectUserDetails);
  const chatList = useAppSelector(selectChatList);

  useEffect(() => {
    dispatch(getChatList());
  }, [dispatch]);

  return (
    <Flex as="section" minH="100vh">
      <Flex
        bg={useColorModeValue("gray.300", "gray.800")}
        boxShadow={useColorModeValue("sm", "sm-dark")}
        flex="1"
        maxW={{ base: "full", sm: "xs" }}
        py={{ base: "6", sm: "8" }}
        px={{ base: "4", sm: "6" }}
      >
        <Stack justify="space-between" spacing="1">
          <Stack spacing={{ base: "5", sm: "6" }} shouldWrapChildren>
            <UserProfile
              name={user.username}
              image="https://gitlab.com/uploads/-/system/user/avatar/2445011/avatar.png"
              email="hi@sha.codes"
            />
            <Divider />
            <Filter label="Find or start a conversation" />
            <NavButton label="Friends" icon={faUsers} path="/" />
            <Stack spacing="1">
              <Text
                color={useColorModeValue("gray.100", "gray.400")}
                fontSize="sm"
                textTransform="uppercase"
              >
                Direct Messages
              </Text>
              {chatList.map((chat) => (
                <NavButton
                  closeable
                  key={chat._id}
                  label={chat.usernames}
                  icon={faUser}
                  path={`/chat/${chat._id}`}
                />
              ))}
            </Stack>
          </Stack>
          <Stack spacing={{ base: "5", sm: "6" }}>
            <Stack spacing="1">
              <NavButton label="Help" icon={faQuestionCircle} path="/" />
              <NavButton label="Settings" icon={faCog} path="/settings" />
              <NavButton
                label="Log out"
                icon={faArrowRightFromBracket}
                path="/logout"
              />
            </Stack>
          </Stack>
        </Stack>
      </Flex>
    </Flex>
  );
};
