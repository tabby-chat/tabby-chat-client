import { extendTheme } from "@chakra-ui/react";

import "@fontsource/rubik";

const theme = extendTheme({
  initialColorMode: "dark",
  useSystemColorMode: false,
  colors: {
    gray: {
      "50": "#F5F5F5",
      "100": "#EBEAEB",
      "200": "#D1D1D1",
      "300": "#B8B7B8",
      "400": "#949394",
      "500": "#5E5D5E",
      "600": "#555455",
      "700": "#4A4A4A",
      "800": "#383838",
      "900": "#262626",
    },
  },
  components: {
    Button: {
      baseStyle: {
        fontWeight: "medium",
      },
    },
  },
  fonts: {
    heading: "Rubik",
    body: "Rubik",
  },
});

export default theme;
