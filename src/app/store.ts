import { configureStore, ThunkAction, Action } from "@reduxjs/toolkit";

import chatReducer from "../features/Chat/chatSlice";
import chatListReducer from "../features/ChatList/chatListSlice";
import friendListReducer from "../features/FriendList/friendListSlice";
import socketReducer from "../features/Socket/socketSlice";
import userReducer from "../features/User/userSlice";

export const store = configureStore({
  reducer: {
    chat: chatReducer,
    chatList: chatListReducer,
    friendList: friendListReducer,
    socket: socketReducer,
    user: userReducer,
  },
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;
