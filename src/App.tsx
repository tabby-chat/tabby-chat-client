import { Helmet } from "react-helmet";
import { Navigate, Route, Routes, useLocation } from "react-router-dom";
import React from "react";

import Layout from "./layout/Layout";
import ChatPage from "./features/Chat/ChatPage";
import FriendListPage from "./features/FriendList/FriendListPage";
import LoginPage from "./features/Login/LoginPage";
import SettingsPage from "./features/Settings/SettingsPage";

import { useAppDispatch, useAppSelector } from "./app/hooks";
import { logout, selectIsAuthenticated } from "./features/User/userSlice";

function Logout() {
  const dispatch = useAppDispatch();
  dispatch(logout());

  return <Navigate to="/entry" replace />;
}

function RequireAuth({ children }: { children: JSX.Element }) {
  const isAuthenticated = useAppSelector(selectIsAuthenticated);
  const location = useLocation();

  if (!isAuthenticated) {
    return <Navigate to="/entry" state={{ from: location }} replace />;
  }
  return children;
}

function App() {
  return (
    <>
      <Helmet>
        <title>Tabby Chat</title>
      </Helmet>
      <Routes>
        <Route element={<LoginPage />} path="/entry" />
        <Route
          element={
            <RequireAuth>
              <Layout />
            </RequireAuth>
          }
          path="/"
        >
          <Route element={<FriendListPage />} index />
          <Route element={<ChatPage />} path="/chat/:chatId" />
          <Route element={<SettingsPage />} path="/settings" />
          <Route element={<Logout />} path="/logout" />
        </Route>
      </Routes>
    </>
  );
}

export default App;
