import { createSlice, PayloadAction } from "@reduxjs/toolkit";

import axios from "../../app/axios-config";
import { AppThunk, RootState } from "../../app/store";
import { User as UserType } from "../Chat/chatSlice";

export type Chat = {
  _id: string;
  users: {
    _id: string;
    username: string;
  }[];
  usernames: string;
};

export interface ChatListState {
  list: Chat[];
}

const initialState: ChatListState = {
  list: [],
};

export const chatListSlice = createSlice({
  name: "chatList",
  initialState,
  reducers: {
    updateChatList: (state, action: PayloadAction<Chat[]>) => {
      state.list = action.payload;
    },
  },
});

export const { updateChatList } = chatListSlice.actions;

export const selectChatList = (state: RootState) => state.chatList.list;

/*
  TODO: Currently retriving all chats, but this will need to be filtered by user.
 */
export const getChatList = (): AppThunk => (dispatch) => {
  axios
    .get("/chat/list")
    .then((response) => {
      dispatch(
        updateChatList(
          response.data.map((chat: { _id: string; users: UserType[] }) => {
            return {
              ...chat,
              usernames: chat.users.map((user) => user.username).join(", "),
            };
          })
        )
      );
    })
    .catch((err) => {
      console.log(err);
    });
};

export default chatListSlice.reducer;
