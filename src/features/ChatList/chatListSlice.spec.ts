import chatlistReducer from "./chatListSlice";

describe("chatlist reducer", () => {
  it("should handle initial state", () => {
    expect(chatlistReducer(undefined, { type: "unknown" })).toEqual({
      list: [],
    });
  });
});
