import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  id: "",
  connected: false,
};

export const socketSlice = createSlice({
  name: "socket",
  initialState,
  reducers: {
    updateSocket: (state, action) => {
      const { connected, id } = action.payload;
      state.id = id;
      state.connected = connected;
    },
  },
});

export const { updateSocket } = socketSlice.actions;

export default socketSlice.reducer;
