import { Box } from "@chakra-ui/react";
import React, { useEffect } from "react";
import { io } from "socket.io-client";

import { useAppDispatch, useAppSelector } from "../../app/hooks";
import { receiveMessage } from "../Chat/chatSlice";
import { updateSocket } from "./socketSlice";
import { selectUserDetails } from "../User/userSlice";

function Socket() {
  const dispatch = useAppDispatch();
  const user = useAppSelector(selectUserDetails);

  useEffect(() => {
    const socket = io("http://localhost:8080");
    socket.on("connect", () => {
      dispatch(updateSocket({ connected: socket.connected, id: socket.id }));
      socket.emit("updateSocket", { socketId: socket.id, userId: user._id });
    });

    socket.on("receiveMessage", (response) => {
      dispatch(receiveMessage(response));
    });
  }, []);

  return <Box></Box>;
}

export default Socket;
