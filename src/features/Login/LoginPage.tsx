import {
  Box,
  Container,
  Flex,
  Heading,
  Stack,
  Text,
  useColorModeValue,
} from "@chakra-ui/react";
import React, { useEffect } from "react";
import { useNavigate } from "react-router-dom";

import CreateUserForm from "./components/CreateUserForm";
import LoginForm from "./components/LoginForm";

import { useAppDispatch } from "../../app/hooks";
import { loginUser, getUserList } from "../User/userSlice";

function LoginPage() {
  const dispatch = useAppDispatch();
  const navigate = useNavigate();

  function handleLogin(userId: string) {
    dispatch(loginUser(userId, navigate));
  }

  useEffect(() => {
    dispatch(getUserList());
  }, [dispatch]);

  return (
    <Flex minH="100vh">
      <Box
        flex="1"
        bg={useColorModeValue("gray.100", "gray.900")}
        px="8"
        py="8"
      >
        <Flex align="center" justify="center" h="full">
          <Box py={{ base: "12", md: "24" }}>
            <Container
              bg={useColorModeValue("gray.300", "gray.800")}
              boxShadow={useColorModeValue("sm", "sm-dark")}
              borderRadius={{ base: "none", sm: "xl" }}
              maxW="xl"
              px={{ base: "4", sm: "10" }}
              py={{ base: "0", sm: "8" }}
            >
              <Stack spacing={8}>
                <Stack spacing={6} align="center">
                  <Stack spacing={3}>
                    <Heading size="xl" textAlign="center">
                      Tabby Chat
                    </Heading>
                    <Text fontSize="sm">
                      First, create some users. Then select one to login with.
                    </Text>
                    <Text fontSize="sm">
                      A user will need to exist before you can add them as a
                      friend.
                    </Text>
                  </Stack>
                  <CreateUserForm />
                  <LoginForm handleLogin={handleLogin} />
                </Stack>
              </Stack>
            </Container>
          </Box>
        </Flex>
      </Box>
    </Flex>
  );
}

export default LoginPage;
