import { Button, SimpleGrid } from "@chakra-ui/react";
import React from "react";

import { useAppSelector } from "../../../app/hooks";
import { selectUserList } from "../../User/userSlice";

function LoginForm({ handleLogin }: { handleLogin: Function }) {
  const userList = useAppSelector(selectUserList);

  return (
    <SimpleGrid columns={2} spacing={4}>
      {userList.map((user) => (
        <Button
          colorScheme="blue"
          fontSize="md"
          key={user._id}
          onClick={() => handleLogin(user._id)}
          size="lg"
        >
          Sign as {user.username}
        </Button>
      ))}
    </SimpleGrid>
  );
}

export default LoginForm;
