import { Button, HStack, Input, useColorModeValue } from "@chakra-ui/react";
import React, { ChangeEvent, KeyboardEvent, useState } from "react";

import { useAppDispatch } from "../../../app/hooks";
import { createUser } from "../../User/userSlice";

function CreateUserForm() {
  const dispatch = useAppDispatch();

  const [form, setForm] = useState<{ username: string }>({ username: "" });

  function handleCreateUser() {
    dispatch(createUser(form.username));
    setForm({ username: "" });
  }

  const handleEnterSubmit = (event: KeyboardEvent<HTMLInputElement>) => {
    if (event.key === "Enter") {
      handleCreateUser();
    }
  };

  const handleInputChange = (event: ChangeEvent<HTMLInputElement>) => {
    setForm({ ...form, [event.target.name]: event.target.value });
  };

  return (
    <HStack>
      <Input
        bg={useColorModeValue("gray.200", "gray.900")}
        borderColor="transparent"
        name="username"
        onChange={handleInputChange}
        onKeyPress={handleEnterSubmit}
        value={form["username"]}
      />
      <Button fontSize="md" onClick={() => handleCreateUser()}>
        Create
      </Button>
    </HStack>
  );
}

export default CreateUserForm;
