import { Box, Container, Stack, Text } from "@chakra-ui/react";
import React from "react";

import { ColorModeSwitcher } from "../../components";

const SettingsPage = () => {
  return (
    <Box as="section" pt={{ base: "4", md: "8" }}>
      <Container>
        <Stack spacing={8}>
          <ColorModeSwitcher />
          <Text>More stuff will go here.</Text>
        </Stack>
      </Container>
    </Box>
  );
};

export default SettingsPage;
