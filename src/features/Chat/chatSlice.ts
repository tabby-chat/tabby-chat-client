import { createSlice, PayloadAction } from "@reduxjs/toolkit";

import axios from "../../app/axios-config";
import { AppThunk, RootState } from "../../app/store";
import { getChatList } from "../ChatList/chatListSlice";

export type User = {
  _id: string;
  username: string;
};

export type Message = {
  content: string;
  from: User;
  _id: string;
  time: string;
};

export interface ChatState {
  _id: string;
  messages: Message[];
  users: User[];
}

const initialState: ChatState = {
  _id: "",
  messages: [],
  users: [],
};

export const chatSlice = createSlice({
  name: "chat",
  initialState,
  reducers: {
    addMessage: (state, action: PayloadAction<Message>) => {
      state.messages = [action.payload, ...state.messages];
    },
    userChanged: (state, action: PayloadAction<User[]>) => {
      state.users = action.payload;
    },
    updateChat: (state, action: PayloadAction<ChatState>) => {
      const { _id, messages, users } = action.payload;
      state._id = _id;
      state.messages = messages;
      state.users = users;
    },
  },
});

export const { addMessage, updateChat, userChanged } = chatSlice.actions;

export const selectChat = (state: RootState) => state.chat;

export const selectMessages = (state: RootState) => state.chat.messages;

export const receiveMessage =
  (updatedChat: ChatState): AppThunk =>
  (dispatch, getState) => {
    const currentChat = selectChat(getState());
    if (updatedChat._id === currentChat._id) {
      dispatch(updateChat(updatedChat));
    }
  };

export const sendMessage =
  (
    chatId: string,
    message: {
      content: string;
      from: string;
      time: string;
    }
  ): AppThunk =>
  (dispatch) => {
    axios
      .put(`/chat/sendMessage/${chatId}`, message)
      .then((response) => {
        // TODO:
      })
      .catch((err) => {
        console.log(err);
      });
  };

export const getChatById =
  (chatId: string | undefined): AppThunk =>
  (dispatch) => {
    axios
      .get("/chat/byChatId", {
        params: {
          chatId,
        },
      })
      .then((response) => {
        dispatch(updateChat(response.data));
      })
      .catch((err) => {
        console.log(err);
      });
  };

export const getChatIdByUserId =
  (
    { currentUserId, friendId }: { currentUserId: string; friendId: string },
    navigate: any
  ): AppThunk =>
  (dispatch) => {
    axios
      .get("/chat/byUserId", {
        params: {
          currentUserId,
          friendId,
        },
      })
      .then((response) => {
        dispatch(getChatList());
        navigate(`/chat/${response.data}`);
      })
      .catch((err) => {
        console.log(err);
      });
  };

export default chatSlice.reducer;
