import { Box, Flex } from "@chakra-ui/react";
import React from "react";

import Message from "./Message";

import { Message as MessageType } from "../chatSlice";

export const MessageList = ({ messages }: { messages: MessageType[] }) => (
  <Flex alignItems="flex-end" h="100%">
    <Box w="100%">
      {messages.map((message) => (
        <Message key={message._id} message={message} />
      ))}
    </Box>
  </Flex>
);
