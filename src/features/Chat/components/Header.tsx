import {
  Box,
  Divider,
  HStack,
  IconButton,
  Stack,
  Text,
} from "@chakra-ui/react";
import { faPhoneVolume, faVideo } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React from "react";

import { User as UserType } from "../chatSlice";

export const Header = ({ users }: { users: UserType[] }) => {
  return (
    <Box as="section" bg="bg-surface" pt={4} pb={{ base: "12", md: "24" }}>
      <Stack spacing={5}>
        <Stack
          alignItems="center"
          direction={{ base: "column", sm: "row" }}
          justify="space-between"
          spacing={4}
        >
          <Text fontSize="lg" fontWeight="medium">
            {users.map((user) => user.username).join(", ")}
          </Text>
          <HStack spacing={2}>
            <IconButton
              alignSelf="start"
              aria-label="Start a voice call"
              icon={<FontAwesomeIcon icon={faPhoneVolume} />}
              isRound
            />
            <IconButton
              alignSelf="start"
              aria-label="Start a video call"
              icon={<FontAwesomeIcon icon={faVideo} />}
              isRound
            />
          </HStack>
        </Stack>
        <Divider />
      </Stack>
    </Box>
  );
};
