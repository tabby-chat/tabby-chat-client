import { Box, Divider, Flex, Text, useColorModeValue } from "@chakra-ui/react";
import React from "react";

import { Message as MessageType } from "../chatSlice";

function Message({ message }: { message: MessageType }) {
  return (
    <>
      <Divider />
      <Box my={4}>
        <Flex alignItems="flex-end">
          <Text fontWeight="medium" mr={2}>
            {message.from.username}
          </Text>
          <Text
            color={useColorModeValue("gray.300", "gray.400")}
            fontSize="xs"
            pb={0.5}
          >
            {new Date(message.time).toLocaleDateString()}{" "}
            {new Date(message.time).toLocaleTimeString()}
          </Text>
        </Flex>
        <Text
          color={useColorModeValue("gray.400", "gray.200")}
          fontWeight="light"
        >
          {message.content}
        </Text>
      </Box>
    </>
  );
}

export default Message;
