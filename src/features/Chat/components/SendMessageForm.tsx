import {
  Divider,
  IconButton,
  Input,
  InputGroup,
  InputLeftElement,
  InputRightElement,
  useColorModeValue,
} from "@chakra-ui/react";
import {
  faCirclePlus,
  faCircleRight,
  faFaceSmile,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { ChangeEvent, KeyboardEvent, useState } from "react";

import { useAppDispatch } from "../../../app/hooks";
import { sendMessage, User as UserType } from "../chatSlice";

export const SendMessageForm = ({
  chatId,
  userId,
  users,
}: {
  chatId: string;
  userId: string;
  users: UserType[];
}) => {
  const dispatch = useAppDispatch();

  const [form, setForm] = useState<{ message: string }>({ message: "" });

  const handleEnterSubmit = (event: KeyboardEvent<HTMLInputElement>) => {
    if (event.key === "Enter") {
      handleSendMessage();
    }
  };

  const handleInputChange = (event: ChangeEvent<HTMLInputElement>) => {
    setForm({ ...form, [event.target.name]: event.target.value });
  };

  const handleSendMessage = () => {
    dispatch(
      sendMessage(chatId, {
        content: form.message,
        from: userId,
        time: new Date().toString(),
      })
    );
    setForm({ message: "" });
  };

  return (
    <InputGroup>
      <InputLeftElement>
        <IconButton
          aria-label="Attach a file"
          icon={<FontAwesomeIcon icon={faCirclePlus} />}
          isRound
          variant="ghost"
        />
      </InputLeftElement>
      <Input
        bg={useColorModeValue("gray.200", "gray.900")}
        borderColor="transparent"
        name="message"
        onChange={handleInputChange}
        onKeyPress={handleEnterSubmit}
        placeholder={`Message ${users.map((user) => user.username).join(", ")}`}
        value={form["message"]}
      />
      <InputRightElement mr={5}>
        <>
          <IconButton
            aria-label="Pick an emoji"
            icon={<FontAwesomeIcon icon={faFaceSmile} />}
            isRound
            variant="ghost"
          />
          <Divider orientation="vertical" />
          <IconButton
            aria-label="Send message"
            icon={<FontAwesomeIcon icon={faCircleRight} />}
            isRound
            onClick={() => handleSendMessage()}
            variant="ghost"
          />
        </>
      </InputRightElement>
    </InputGroup>
  );
};
