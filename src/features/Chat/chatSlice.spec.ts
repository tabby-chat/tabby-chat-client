import chatReducer, { ChatState, addMessage, updateChat } from "./chatSlice";

describe("chat reducer", () => {
  const initialState: ChatState = {
    _id: "",
    messages: [],
    users: [],
  };
  it("should handle initial state", () => {
    expect(chatReducer(undefined, { type: "unknown" })).toEqual({
      _id: "",
      messages: [],
      users: [],
    });
  });

  it("should add new messages", () => {
    const actual = chatReducer(
      initialState,
      addMessage({
        _id: "messageId01",
        content: "This is a message.",
        from: {
          _id: "userId01",
          username: "guest0001",
        },
        time: "28/02/2022",
      })
    );
    expect(actual.messages[0]).toEqual({
      _id: "messageId01",
      content: "This is a message.",
      from: {
        _id: "userId01",
        username: "guest0001",
      },
      time: "28/02/2022",
    });
  });

  it("should load chat", () => {
    const actual = chatReducer(
      initialState,
      updateChat({
        _id: "chatId01",
        messages: [
          {
            _id: "messageId01",
            content: "This is a message.",
            from: {
              _id: "userId01",
              username: "guest0001",
            },
            time: "28/02/2022",
          },
        ],
        users: [
          {
            _id: "userId01",
            username: "guest0001",
          },
          {
            _id: "userId02",
            username: "guest0002",
          },
        ],
      })
    );
    expect(actual).toEqual({
      _id: "chatId01",
      messages: [
        {
          _id: "messageId01",
          content: "This is a message.",
          from: {
            _id: "userId01",
            username: "guest0001",
          },
          time: "28/02/2022",
        },
      ],
      users: [
        {
          _id: "userId01",
          username: "guest0001",
        },
        {
          _id: "userId02",
          username: "guest0002",
        },
      ],
    });
  });
});
