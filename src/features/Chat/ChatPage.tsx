import { Flex, Stack, useBreakpointValue } from "@chakra-ui/react";
import React, { useEffect } from "react";
import { useParams } from "react-router-dom";

import { Loading } from "../../components";
import { Header } from "./components/Header";
import { MessageList } from "./components/MessageList";
import { SendMessageForm } from "./components/SendMessageForm";

import { useAppDispatch, useAppSelector } from "../../app/hooks";
import { getChatById, selectChat } from "./chatSlice";
import { selectUserDetails } from "../User/userSlice";

function ChatPage() {
  const dispatch = useAppDispatch();
  const chat = useAppSelector(selectChat);
  const user = useAppSelector(selectUserDetails);

  const isDesktop = useBreakpointValue({ base: false, lg: true });

  const params = useParams();

  useEffect(() => {
    dispatch(getChatById(params.chatId));
  }, []);

  if (!chat._id) {
    return <Loading />;
  }

  return (
    <Flex
      as="section"
      maxH={isDesktop ? "98vh" : "93vh"}
      minH={isDesktop ? "98vh" : "93vh"}
      w="full"
    >
      <Flex
        flex="1"
        py={{ base: "6", sm: "8", lg: "2" }}
        px={{ base: "4", sm: "6" }}
        w="100%"
      >
        <Stack justify="space-between" spacing="1" w="100%">
          <Header users={chat.users} />
          <MessageList messages={chat.messages} />
          <SendMessageForm
            chatId={chat._id}
            userId={user._id}
            users={chat.users}
          />
        </Stack>
      </Flex>
    </Flex>
  );
}

export default ChatPage;
