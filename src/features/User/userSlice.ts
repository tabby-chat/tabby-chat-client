import { createSlice } from "@reduxjs/toolkit";
import { Dispatch } from "redux";
import { AppThunk, RootState } from "../../app/store";

import axios from "../../app/axios-config";
import { getFriendList } from "../FriendList/friendListSlice";

export type User = {
  _id: string;
  username: string;
};

export interface UserState {
  isAuthenticated: boolean;
  list: User[];
  details: {
    _id: string;
    username: string;
  };
}

const initialState: UserState = {
  isAuthenticated: false,
  list: [],
  details: {
    _id: "",
    username: "",
  },
};

export const userSlice = createSlice({
  name: "user",
  initialState,
  reducers: {
    authenticate: (state) => {
      state.isAuthenticated = true;
    },
    createdUser: (state, action) => {
      state.list = [...state.list, action.payload];
    },
    logout: (state) => {
      state.isAuthenticated = false;
    },
    updateUser: (state, action) => {
      state.details = action.payload;
    },
    updateUserList: (state, action) => {
      state.list = action.payload;
    },
  },
});

export const { authenticate, createdUser, logout, updateUser, updateUserList } =
  userSlice.actions;

export const selectIsAuthenticated = (state: RootState) =>
  state.user.isAuthenticated;
export const selectUserDetails = (state: RootState) => state.user.details;
export const selectUserList = (state: RootState) => state.user.list;

export const addFriend =
  (friendUsername: string): AppThunk =>
  (dispatch, getState) => {
    const currentUser = selectUserDetails(getState());

    axios
      .post("/user/friends/add", {
        currentUserId: currentUser._id,
        friendUsername,
      })
      .then((response) => {
        dispatch(getFriendList());
        // TODO: Add alert that says friend invite sent.
        // In the future, a friend invite will need to be accepted first.
      })
      .catch((err) => {
        console.log(err);
      });
  };

export const createUser =
  (username: string): AppThunk =>
  (dispatch) => {
    axios
      .post("/user/create", {
        username,
      })
      .then((response) => {
        dispatch(createdUser(response.data));
      })
      .catch((err) => {
        console.log(err);
      });
  };

export const getUserDetails =
  (userId: string): AppThunk =>
  (dispatch) => {
    axios
      .get(`/user/${userId}`)
      .then((response) => {
        dispatch(updateUser(response.data));
      })
      .catch((err) => {
        console.log(err);
      });
  };

export const getUserList = (): AppThunk => (dispatch) => {
  axios
    .get("/user/list")
    .then((response) => {
      dispatch(updateUserList(response.data));
    })
    .catch((err) => {
      console.log(err);
    });
};

export const loginUser =
  (userId: string, navigate: any): AppThunk =>
  (dispatch) => {
    axios
      .post("/auth/login", {
        userId,
      })
      .then((response) => {
        dispatch(authenticate());
        dispatch(updateUser(response.data));
        navigate("/");
      })
      .catch((err) => {
        console.log(err);
      });
  };

export default userSlice.reducer;
