import userReducer, { UserState } from "./userSlice";

describe("user reducer", () => {
  const initialState: UserState = {
    isAuthenticated: false,
    list: [],
    details: {
      _id: "",
      username: "",
    },
  };

  it("should handle initial state", () => {
    expect(userReducer(undefined, { type: "unknown" })).toEqual({
      isAuthenticated: false,
      list: [],
      details: {
        _id: "",
        username: "",
      },
    });
  });
});
