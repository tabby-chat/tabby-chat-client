import {
  Box,
  Container,
  Stack,
  Text,
  useColorModeValue,
} from "@chakra-ui/react";
import { faSearch } from "@fortawesome/free-solid-svg-icons";
import React, { useEffect } from "react";
import { useNavigate } from "react-router-dom";

import { Filter } from "../../components";
import AddFriendModal from "./components/AddFriendModal";
import Friend from "./components/Friend";

import { useAppDispatch, useAppSelector } from "../../app/hooks";
import { getChatIdByUserId } from "../Chat/chatSlice";
import { getFriendList, selectFriendList } from "./friendListSlice";
import { selectUserDetails } from "../User/userSlice";

function FriendListPage() {
  const dispatch = useAppDispatch();
  const navigate = useNavigate();
  const friendList = useAppSelector(selectFriendList);
  const userDetails = useAppSelector(selectUserDetails);

  useEffect(() => {
    dispatch(getFriendList());
  }, [dispatch]);

  function handleGetChatIdByUserId(userId: string) {
    dispatch(
      getChatIdByUserId(
        { currentUserId: userDetails._id, friendId: userId },
        navigate
      )
    );
  }

  return (
    <Box as="section" pt={{ base: "4", md: "8" }}>
      <Container>
        <Stack spacing={8}>
          <Filter icon={faSearch} label="Search" />
          <Box>
            <Text
              color={useColorModeValue("gray.300", "gray.400")}
              textTransform="uppercase"
            >
              Online
            </Text>
            {friendList.length > 0 &&
              friendList.map((friend) => (
                <Friend
                  handleGetChatByUserId={handleGetChatIdByUserId}
                  key={friend.username}
                  user={friend}
                />
              ))}
          </Box>
          <AddFriendModal />
        </Stack>
      </Container>
    </Box>
  );
}

export default FriendListPage;
