import {
  Button,
  FormControl,
  FormLabel,
  Input,
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
  useColorModeValue,
  useDisclosure,
} from "@chakra-ui/react";
import { faUserPlus } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { ChangeEvent, KeyboardEvent, useState } from "react";

import { useAppDispatch } from "../../../app/hooks";
import { addFriend } from "../../User/userSlice";

const AddFriendModal = () => {
  const dispatch = useAppDispatch();
  const { isOpen, onOpen, onClose } = useDisclosure();

  const initialRef = React.useRef<HTMLInputElement>(null);

  const [form, setForm] = useState<{ username: string }>({ username: "" });

  function handleAddFriend() {
    dispatch(addFriend(form.username));
    setForm({ username: "" });
    onClose();
  }

  const handleEnterSubmit = (event: KeyboardEvent<HTMLInputElement>) => {
    if (event.key === "Enter") {
      handleAddFriend();
    }
  };

  const handleInputChange = (event: ChangeEvent<HTMLInputElement>) => {
    setForm({ ...form, [event.target.name]: event.target.value });
  };

  return (
    <>
      <Button leftIcon={<FontAwesomeIcon icon={faUserPlus} />} onClick={onOpen}>
        Add friend
      </Button>

      <Modal initialFocusRef={initialRef} isOpen={isOpen} onClose={onClose}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>Add a friend</ModalHeader>
          <ModalCloseButton />
          <ModalBody pb={6}>
            <FormControl>
              <FormLabel>Username</FormLabel>
              <Input
                bg={useColorModeValue("gray.200", "gray.900")}
                borderColor="transparent"
                name="username"
                onChange={handleInputChange}
                onKeyPress={handleEnterSubmit}
                ref={initialRef}
                value={form["username"]}
              />
            </FormControl>
          </ModalBody>

          <ModalFooter>
            <Button mr={3} onClick={handleAddFriend}>
              Add friend
            </Button>
            <Button onClick={onClose}>Cancel</Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  );
};

export default AddFriendModal;
