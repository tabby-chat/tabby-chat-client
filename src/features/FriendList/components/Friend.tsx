import {
  Box,
  Button,
  Flex,
  HStack,
  IconButton,
  Spacer,
  Text,
  useColorModeValue,
} from "@chakra-ui/react";
import {
  faEllipsisV,
  faMessage,
  faUser,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React from "react";
import { Link as RouterLink } from "react-router-dom";

type FriendProps = {
  handleGetChatByUserId: Function;
  user: {
    _id: string;
    username: string;
  };
};

const Friend = ({ handleGetChatByUserId, user }: FriendProps) => {
  return (
    <Button
      borderTop="1px"
      borderColor={useColorModeValue("gray.300", "gray.500")}
      borderRadius="none"
      justifyContent="flex-start"
      leftIcon={<FontAwesomeIcon icon={faUser} />}
      my={0}
      onClick={() => handleGetChatByUserId(user._id)}
      py={8}
      variant="ghost"
      w="full"
    >
      <Flex w="full">
        <Box>
          <Text py={4}>{user.username}</Text>
        </Box>
        <Spacer />
        {/* <HStack>
          <IconButton
            aria-label="Search database"
            icon={<FontAwesomeIcon icon={faMessage} />}
            isRound
          />
          <IconButton
            aria-label="Search database"
            icon={<FontAwesomeIcon icon={faEllipsisV} />}
            isRound
          />
        </HStack> */}
      </Flex>
    </Button>
  );
};

export default Friend;
