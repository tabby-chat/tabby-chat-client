import { createSlice, PayloadAction } from "@reduxjs/toolkit";

import axios from "../../app/axios-config";
import { AppThunk, RootState } from "../../app/store";

export type Friend = {
  _id: string;
  username: string;
};

export interface FriendListState {
  list: Friend[];
}

const initialState: FriendListState = {
  list: [],
};

export const friendListSlice = createSlice({
  name: "friendList",
  initialState,
  reducers: {
    updateFriendList: (state, action: PayloadAction<Friend[]>) => {
      state.list = action.payload;
    },
  },
});

export const { updateFriendList } = friendListSlice.actions;

export const selectFriendList = (state: RootState) => state.friendList.list;
export const selectUser = (state: RootState) => state.user.details;

export const getFriendList = (): AppThunk => (dispatch, getState) => {
  const currentUser = selectUser(getState());
  axios
    .get(`/user/friends/list/${currentUser._id}`)
    .then((response) => {
      dispatch(updateFriendList(response.data));
    })
    .catch((err) => {
      console.log(err);
    });
};

export default friendListSlice.reducer;
