import friendlistReducer from "./friendListSlice";

describe("friendlist reducer", () => {
  it("should handle initial state", () => {
    expect(friendlistReducer(undefined, { type: "unknown" })).toEqual({
      list: [],
    });
  });
});
