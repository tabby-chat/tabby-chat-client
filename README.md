# Tabby Chat

Puns for days.

Tab:

1. a second or further document or page that can be opened on a spreadsheet or web browser.

Tabby:

1. a grey or brownish cat mottled or streaked with dark stripes.

Chat:

1. the online exchange of messages in real time with one or more simultaneous users of a computer network.
2. "cat" in French.

Fun fact: the shades of grey used in the design are taken from my cat's coat (and desaturated a little).

# Outline

The server runs on Node and Express. It connects to a MongoDB database (via mongoose) with two collections: chats and users. It uses a combination of socket.io and node-cache to handle the sending and receiving of messages. For convenience, it has nodemon for running `npm run dev`.

The client uses React, React Router DOM for routing, and Redux Toolkit for managing its 5 reducers: chat, chatList, friendList, socket, and user. It uses ChakraUI for the UI, FontAwesome for its icons, axios for its HTTP calls, and socket.io for the sending and receiving of messages.

# Setup

1. Add the .env file for the server. This will allow connecting to the MongoDB for storing users, their friends, and chats.
2. Run the server.
3. Run the client for as many users as you want to simulate. (Currently only tested for two.)
4. Create the users with the simple form if there are none.
5. Login as those users on each client. (You will need to refresh the page to update the user list.)
6. Add each other as friends in each client. Adding a user as a friend will not make you a friend of that user. (Implementing friend request approval is a TO DO. This is when bidirectional friend-adding will be implemented.)
7. Click the user in the list to create a chat tab with them for each user involved.
8. Chat with each other.

# To Do

There are a lot of TO DO items in this early stage of the web app...
